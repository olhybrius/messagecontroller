package com.example.multiformat.domain.entities;

import java.util.Arrays;
import java.util.Optional;

public enum MediaType {
    APPLICATION_JSON("application/json"),
    APPLICATION_XML("application/xml");

    public static final String APPLICATION_JSON_VALUE = "application/json";
    public static final String APPLICATION_XML_VALUE = "application/xml";

    private final String value;

    MediaType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Optional<MediaType> of(String value) {
        return Arrays.stream(MediaType.values()).filter(mediaType -> mediaType.value.equals(value)).findFirst();
    }
}
