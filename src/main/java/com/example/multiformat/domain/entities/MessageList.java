package com.example.multiformat.domain.entities;

import java.util.Map;

public record MessageList(Map<String, MessageDetails> messages) {
}
