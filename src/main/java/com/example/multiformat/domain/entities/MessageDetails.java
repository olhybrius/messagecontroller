package com.example.multiformat.domain.entities;

public record MessageDetails(String message, String tenantId) {
}
