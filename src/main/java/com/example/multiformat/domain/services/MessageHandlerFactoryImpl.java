package com.example.multiformat.domain.services;

import com.example.multiformat.domain.entities.MediaType;
import com.example.multiformat.domain.ports.primary.MessageHandlerFactory;
import com.example.multiformat.domain.ports.secondary.MessageRepository;

public class MessageHandlerFactoryImpl implements MessageHandlerFactory {

    private final MessageRepository messageRepository;

    public MessageHandlerFactoryImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public MessageHandler getMessageHandler(MediaType mediaType) {
        if (mediaType.equals(MediaType.APPLICATION_JSON)) return new JsonMessageHandler(messageRepository);
        if (mediaType.equals(MediaType.APPLICATION_XML)) return new XmlMessageHandler(messageRepository);
        throw new IllegalArgumentException("MediaType non-géré");
    }
}
