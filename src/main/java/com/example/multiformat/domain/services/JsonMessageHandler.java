package com.example.multiformat.domain.services;

import com.example.multiformat.domain.entities.MessageList;
import com.example.multiformat.domain.ports.secondary.MessageRepository;

public class JsonMessageHandler extends BaseMessageHandler {

    protected JsonMessageHandler(MessageRepository messageRepository) {
        super(messageRepository);
    }

    @Override
    public void handle(MessageList messageList) {
        super.handle(messageList);
        System.out.println("JSON : " + messageList);
    }
}
