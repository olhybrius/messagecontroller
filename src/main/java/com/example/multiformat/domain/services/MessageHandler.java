package com.example.multiformat.domain.services;

import com.example.multiformat.domain.entities.MessageList;
import com.example.multiformat.domain.entities.MessageDetails;

import java.util.List;
import java.util.Map;

public interface MessageHandler {
    void handle(MessageList message);

    Map<String, Long> getMessagesCountPerUser();

    List<MessageDetails> getAllMessages();

    MessageDetails getMessage(String id);
}
