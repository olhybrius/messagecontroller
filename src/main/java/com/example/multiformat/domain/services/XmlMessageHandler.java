package com.example.multiformat.domain.services;

import com.example.multiformat.domain.entities.MessageList;
import com.example.multiformat.domain.ports.secondary.MessageRepository;

public class XmlMessageHandler extends BaseMessageHandler {
    protected XmlMessageHandler(MessageRepository messageRepository) {
        super(messageRepository);
    }

    @Override
    public void handle(MessageList messageList) {
        super.handle(messageList);
        System.out.println("XML :" + messageList);
    }
}
