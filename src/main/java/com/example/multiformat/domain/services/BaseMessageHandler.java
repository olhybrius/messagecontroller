package com.example.multiformat.domain.services;

import com.example.multiformat.domain.entities.MessageDetails;
import com.example.multiformat.domain.entities.MessageList;
import com.example.multiformat.domain.ports.secondary.MessageRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class BaseMessageHandler implements MessageHandler {

    protected final MessageRepository messageRepository;

    protected BaseMessageHandler(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public void handle(MessageList message) {
        messageRepository.save(message);
    }

    @Override
    public final Map<String, Long> getMessagesCountPerUser() {
        List<MessageDetails> messages = getAllMessages();
        Stream<MessageDetails> messageDetailsList = messages.stream();
        return messageDetailsList.collect(Collectors.groupingBy(MessageDetails::tenantId, Collectors.counting()));
    }

    @Override
    public final List<MessageDetails> getAllMessages() {
        return messageRepository.findAll();
    }

    @Override
    public final MessageDetails getMessage(String id) {
        return messageRepository.findById(id);
    }
}
