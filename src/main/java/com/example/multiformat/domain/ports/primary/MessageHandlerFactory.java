package com.example.multiformat.domain.ports.primary;

import com.example.multiformat.domain.entities.MediaType;
import com.example.multiformat.domain.services.MessageHandler;

public interface MessageHandlerFactory {
     MessageHandler getMessageHandler(MediaType mediaType);
}
