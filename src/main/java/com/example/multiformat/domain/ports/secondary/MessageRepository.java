package com.example.multiformat.domain.ports.secondary;

import com.example.multiformat.domain.entities.MessageDetails;
import com.example.multiformat.domain.entities.MessageList;

import java.util.List;

public interface MessageRepository {
    void save(MessageList messageList);
    MessageDetails findById(String id);
    List<MessageDetails> findAll();
}
