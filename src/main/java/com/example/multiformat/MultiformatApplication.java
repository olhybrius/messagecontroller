package com.example.multiformat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiformatApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiformatApplication.class, args);
    }

}
