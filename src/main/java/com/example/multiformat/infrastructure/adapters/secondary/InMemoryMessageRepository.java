package com.example.multiformat.infrastructure.adapters.secondary;

import com.example.multiformat.domain.entities.MessageDetails;
import com.example.multiformat.domain.entities.MessageList;
import com.example.multiformat.domain.ports.secondary.MessageRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Repository
public class InMemoryMessageRepository implements MessageRepository {

    private final MessageList messageList;

    public InMemoryMessageRepository() {
        this.messageList = new MessageList(new HashMap<>());
    }

    @Override
    public void save(MessageList messageList) {
        messageList.messages().forEach((key, value) -> this.messageList.messages().put(key, value));
    }

    @Override
    public MessageDetails findById(String id) {
        return Optional.ofNullable(this.messageList.messages().get(id))
                .orElseThrow(() -> new NoSuchElementException("Message introuvable"));
    }

    @Override
    public List<MessageDetails> findAll() {
        return messageList.messages().values().stream().toList();
    }
}
