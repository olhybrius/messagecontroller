package com.example.multiformat.infrastructure.adapters.primary;

import com.example.multiformat.domain.entities.MediaType;
import com.example.multiformat.domain.entities.MessageDetails;
import com.example.multiformat.domain.entities.MessageList;
import com.example.multiformat.domain.services.MessageHandler;
import com.example.multiformat.domain.ports.primary.MessageHandlerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class MessageController {
    private final MessageHandlerFactory messageHandlerFactory;

    public MessageController(MessageHandlerFactory messageHandlerFactory) {
        this.messageHandlerFactory = messageHandlerFactory;
    }

    @PostMapping(value = "/messages", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void sendMessages(HttpServletRequest request, @RequestBody MessageList messageList) {
        String contentType = request.getContentType();
        MediaType mediaType = MediaType.of(contentType).orElseThrow();
        MessageHandler messageHandler = messageHandlerFactory.getMessageHandler(mediaType);
        messageHandler.handle(messageList);

        System.out.println(messageHandler.getMessagesCountPerUser());

        List<MessageDetails> messages = messageHandler.getAllMessages();
        messages.forEach(System.out::println);

        System.out.println(messageHandler.getMessage("23"));
    }
}
