package com.example.multiformat.infrastructure.config;

import com.example.multiformat.domain.ports.primary.MessageHandlerFactory;
import com.example.multiformat.domain.ports.secondary.MessageRepository;
import com.example.multiformat.domain.services.MessageHandlerFactoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.example.multiformat.domain.services")
public class BeanConfiguration {

    @Bean
    MessageHandlerFactory messageHandlerFactory(MessageRepository messageRepository) {
        return new MessageHandlerFactoryImpl(messageRepository);
    }
}
